# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/azurerm" {
  version     = "2.14.0"
  constraints = "~> 2.14.0"
  hashes = [
    "h1:pkTbJFktmjalm2D5KaNAAI4/PP1zwOmoZXb8ngXFDMY=",
    "zh:23c90abf2ca22e8563c3009d796dfdc02f437ab852214d110f6f73a8aa51cfec",
    "zh:23f21ee4cd6a11a0e4814e3a3727d4177af30d6d3d590d2923ecfd7094891306",
    "zh:462c14faad233c94780147dacfd8151aef1fe64b0ac189602b07dca15548a938",
    "zh:4e2d8381d47ba5c74461490240d11a558e6d61e8c11421524ab9c043ce12b554",
    "zh:92d14e09c5e88c3424acb657054e96dc92a54e67bbc85a47f38e30fac85d53db",
    "zh:acd12c956512e00557924193c9e4e48d05933b950b9bc3fc51e134f460a1dd1a",
    "zh:b16247a6dc9d7b042d9148d3e00ad3deb5c8764823a56f115fa55ee0df5e97af",
    "zh:d14fa674b5c3ea5a0145c6276a3580155ebbc6dfa4cbdbf62553f2c0c683c9f2",
    "zh:d8a672c2d3b86ea9ad5ab33cf2af11a05fb29ffa9cc432871fbd001e8c35798d",
    "zh:e1035dcf7f599ca6c84183846ef679efd66f7dca703cbc864a238e78204d967f",
    "zh:ee00eb6ecf11f2cb450d29f35d0fbbf52adf58ccba01828e62fb22dedb88ad45",
    "zh:fbe6fdc2a2af2ea2a474253f51a5cc607afc4461cab11a4874fc316c4b940e4b",
  ]
}
