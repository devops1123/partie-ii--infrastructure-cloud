#  Partie II : sur une infrastructure “Azure Cloud” 

- Kubelet: composant qui fonctionne sur toutes les machines dans le cluster et qui fait des choses comme démarrer des pods et des conteneurs.
- kubectl: Ligne de commande utile pour parler au cluster.
- kubeadm: la commande pour démarrer le cluster et gérer plusieurs cluster (non utilisé)
- Minikube: Pour rémarrer le cluster, un seul cluster localement. 

## Préparation de l’infrastructure

- Tous les conteneurs appartenant à ce réseau peuvent communiquer entre eux via l'adressage IP.
- Un pod est un ou plusieurs conteneurs qui vivent ensemble et partagent un namespaces réseau.

Nous avons choisi Azure Kubernetes Service (AKS) car c'est un service Kubernetes managé qui permet de déployer et de gérer rapidement des clusters. Dans ce démarrage on peut: 
•	Déployez un cluster AKS à l'aide d'Azure CLI/manuel  **(Impérative)** ou alors depuis le script Terraform **(déclarative)**.  
•	Adapter l'intégration avec des systèmes d'approvisionnement comme Terraform ou Ansible.
•	Exécuter une application multi-conteneurs avec une interface Web et une instance Redis dans le cluster.
•	Surveiller la santé du cluster et des pods qui exécutent les applications doli-barr et MariaDB . 
•	Contrôler AZURE IKS  localement sans SSH, on utilise un binaire pour la connexion puis nous pouvons configurer avec l’aide de kubectl .


https://docs.microsoft.com/en-us/azure/aks/kubernetes-walkthrough?toc=%2fcli%2fazure%2ftoc.json  (version Impérative), la version déclarative est sur le script main.tr de terraform dans le github. 

# Préparation des images

Nous allons chercher à créer une image pour le service Dolibarr déjà hébergé sur Docker Hub : https://hub.docker.com/repository/docker/legrand01/docker-dolibarr  
Nous allons chercher à créer une image pour le service MariaDB également : https://hub.docker.com/repository/docker/legrand01/mariadb/general 

Pour chaque service, on va développer les scripts et configuration (Dockerfile, docker-compose.yml, etc.) permettant de :

- créer une image 
- vérifier son bon fonctionnement
- Mettre cette image en ligne. 

### Créer un cluster Kubernetes avec 3 nœuds ( azure IKS ) 


![Screenshot](Picture1.png)    .

# Infrastructure as Code (IaC) avec TERRAFORM
Terraform est un outil d'infrastructure en tant que code (IaC) qui permet de créer, de modifier et de versionner une infrastructure de manière sûre et efficace. Cela inclut des composants de bas niveau tels que les instances de calcul, le stockage et la mise en réseau, ainsi que des composants de haut niveau tels que les entrées DNS, les fonctionnalités SAAS, etc. Terraform peut gérer à la fois les fournisseurs de services existants et les solutions internes personnalisées.


![Screenshot](Picture2.png) 
Source : 

https://learn.hashicorp.com/tutorials/terraform/aks?in=terraform/kubernetes   
https://hub.docker.com/r/hashicorp/terraform 

# Concevoir dolibarr première application dans Kubernetes: Communiquer via les services à travers d’Azure Kubernetes Service AKZ.
Afin de pour pouvoir connecter à travers d’Azure Kubernetes Service AKZ, et à travers des services Azure comme App Service, Machine Learning et Batc, tout d’abord on doit utiliser « Container Registry «  solution chez Azure pour créer, stocker, sécuriser, analyser, répliquez et géreR des images de conteneurs et des artefacts avec une instance entièrement gérée et géo-répliquée.

https://azure.microsoft.com/en-us/services/container-registry/#overview  (version Impérative), la version déclarative est sur le script main.tr de terraform dans le github. 

![Screenshot](Picture3.png) 

# Exécuter des applications sur Azure Kubernetes Service (AKS)

Une fois l’image dolibarr chargée sur Azure Container Registry, et un cluster de Kubernetes créé : 

•	Mettre à jour le fichier manifeste Kubernetes (deployment.yaml)
•	Exécuter une application sur Kubernetes 
•	Essayer l'application

![Screenshot](Picture4.png)

# Déployer un cluster Azure Kubernetes Service à l'aide de Terraform or Azure CLI

![Screenshot](Picture5.jpg) 


# On va détruire l’un des pots avec Azure Ressource Manager Azure

##Tools : Docker , Kubernetes , Ruby , Terraform , et RKT 

Les fonctionnalités de gestion, telles que le contrôle d'accès, les verrous et les balises, pour sécuriser et organiser les ressources après le déploiement, fournissent une couche de gestion qui permet de créer, mettre à jour et supprimer des ressources dans l’abonnement Azure. 

# Finalement on va  mettre en place un systeme de monitoring pour surveiller l’état de santé des serveurs ou des services

- Infrastructure monitoring
- Applications & Microservices
- Application Security
- Digital Experience
- Business Analytics
- Cloud Automation


# Installation

Az login > connecter avec Azure Portal
- terraform init 
- terraform plan
- terraform apply
- kubectl apply -k ./


